package com.example.springboot.models;

import java.util.LinkedList;
import java.util.List;

public class TaskEntity {

    public TaskEntity() {
    }

    public TaskEntity(long id, String name, List<SubTaskEntity> subTask) {
        setId(id);
        setName(name);
        setSubTasks(subTask);
    }

    private long id;

    private String name;

    private List<SubTaskEntity> subTasks = new LinkedList<>();

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSubTasks(List<SubTaskEntity> subTasks) {
        this.subTasks = subTasks;
    }

    public List<SubTaskEntity> getSubTasks() {
        return subTasks;
    }

}