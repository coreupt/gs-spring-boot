package com.example.springboot.models;

public class SubTaskEntity {

    public SubTaskEntity(long id, String name) {
        setId(id);
        setName(name);
    }

    private long id;

    private String name;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}