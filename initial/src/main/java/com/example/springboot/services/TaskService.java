package com.example.springboot.services;

import com.example.springboot.models.TaskEntity;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class TaskService {

    private final List<TaskEntity> entities = new LinkedList<>();

    public List<TaskEntity> getTasks() {
        return entities;
    }

    public TaskEntity getTaskById(long id) {
        for (TaskEntity entity: entities) {
            if (entity.getId() == id) return entity;
        }

        return null;
    }

    public void create(TaskEntity task) {
        entities.add(task);
    }

}