package com.example.springboot;

import com.example.springboot.models.TaskEntity;
import com.example.springboot.services.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController {

	@Autowired
	private TaskService taskService;

	@GetMapping("/tasks")
	public ResponseEntity<List<TaskEntity>> getTasks() {
		return new ResponseEntity<>(taskService.getTasks(), HttpStatus.OK);
	}

	@GetMapping("/task/{id}")
	public ResponseEntity<?> getTask(@PathVariable("id") long id) {
		TaskEntity entity = taskService.getTaskById(id);
		if (entity != null) {
			return new ResponseEntity<>(entity, HttpStatus.OK);
		}

		return new ResponseEntity<>(String.format("Error, Task %d not found!", id), HttpStatus.NOT_FOUND);
	}

	@PostMapping("/task/new")
	public ResponseEntity<Void> addTask(@RequestBody TaskEntity task) {
		taskService.create(task);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}